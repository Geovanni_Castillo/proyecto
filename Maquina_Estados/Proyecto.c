#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char  *estado_siguiente(char *entrada2[], int index, char *estado_actual, char *estado[], char *entrada[]);

int main (){
    //Opciones
    char *entrada[] = {"NO_CARROS", "CARROS_ESTE", "CARROS_NORTE", "AMBOS_LADOS"};
    //Estados
    char *estado[] = {"goNORTE", "waitNORTE","goESTE","waitESTE"};
    char estado_actual[20];

    //Entradas--------Modifica el Usuario------------------
    char *entrada1[] = {"NO_CARROS", "CARROS_ESTE", "CARROS_ESTE", "AMBOS_LADOS", "CARROS_NORTE", "CARROS_NORTE", "CARROS_NORTE", "AMBOS_LADOS", "CARROS_ESTE", "NO_CARROS"};
    int t = sizeof(entrada1) / sizeof(char *);
    //Estado Inicial
    strcpy(estado_actual,"goNORTE");

    printf("Estado Inicial ---> %s \n\n", estado_actual);

    for(int i = 0; i < t; i++){
        strcpy(estado_actual,estado_siguiente(entrada1, i, estado_actual, estado, entrada));
    }

    return 0;

}

char  *estado_siguiente(char *entrada2[], int index, char *estado_actual, char *estado[], char *entrada[]){
    char estado_actual_aux[10];

    if (strcmp(estado_actual,estado[0]) == 0){ //goNORTE
            
        if (strcmp(entrada2[index],entrada[0]) == 0 || strcmp(entrada2[index],entrada[2]) == 0){ //NO_CARROS - CARROS_NORTE
                
            strcpy(estado_actual_aux,estado[0]);
            printf("%s <--- %s ---> %s\n", estado_actual, entrada2[index], estado_actual_aux);
            strcpy(estado_actual,estado_actual_aux);

        }else if (strcmp(entrada2[index], entrada[1])== 0 || strcmp(entrada2[index],entrada[3]) == 0){ //CARROS_ESTE
                
            strcpy(estado_actual_aux,estado[1]);
            printf("%s <--- %s ---> %s\n", estado_actual, entrada2[index], estado_actual_aux);
            strcpy(estado_actual,estado_actual_aux);

        }else{
            printf("Error en goNORTE\n");
        }   
            
    }else if(strcmp(estado_actual,estado[1]) == 0){ //waitNORTE
            
        if (strcmp(entrada2[index],entrada[0]) == 0 || strcmp(entrada2[index],entrada[1]) == 0 || strcmp(entrada2[index],entrada[2]) == 0 || strcmp(entrada2[index],entrada[3]) == 0){ //NO_CARROS - CARROS_ESTE - CARROS_NORTE - AMBOS_LADOS
                
            strcpy(estado_actual_aux,estado[2]);
            printf("%s <--- %s ---> %s\n", estado_actual, entrada2[index], estado_actual_aux);
            strcpy(estado_actual,estado_actual_aux);

        }else{
            printf("Error En waitNORTE\n");
        }

    }else if(strcmp(estado_actual,estado[2]) == 0){ //goESTE
            
        if (strcmp(entrada2[index],entrada[0]) == 0 || strcmp(entrada2[index],entrada[1]) == 0){ //NO_CARROS - CARROS_ESTE
                
            strcpy(estado_actual_aux,estado[2]);
            printf("%s <--- %s ---> %s\n", estado_actual, entrada2[index], estado_actual_aux);
            strcpy(estado_actual,estado_actual_aux);

        }else if (strcmp(entrada2[index],entrada[2]) == 0 || strcmp(entrada2[index],entrada[3]) == 0){ //CARROS_NORTE - AMBOS_LADOS
                
            strcpy(estado_actual_aux,estado[3]);
            printf("%s <--- %s ---> %s\n", estado_actual, entrada2[index], estado_actual_aux);
            strcpy(estado_actual,estado_actual_aux);

        }else{
            printf("Error en goESTE");
        }

    }else if (strcmp(estado_actual,estado[3]) == 0){ //waitESTE
            
        if (strcmp(entrada2[index],entrada[0]) == 0 || strcmp(entrada2[index],entrada[1]) == 0 || strcmp(entrada2[index],entrada[2]) == 0 || strcmp(entrada2[index],entrada[3]) == 0){ //NO_CARROS - CARROS_ESTE - CARROS_NORTE - AMBOS_LADOS
                
            strcpy(estado_actual_aux,estado[0]);
            printf("%s <--- %s ---> %s\n", estado_actual, entrada2[index], estado_actual_aux);
            strcpy(estado_actual,estado_actual_aux);

        }else{
            printf("Error En waitESTE");
        }

    }else{
        printf("***ERROR***\n");
    }

    return estado_actual;
}