#include "maquina.h"
#include <gtest/gtest.h>

TEST(SquareRootTest, PositiveNos){
    //ESTRADAS
    const char *entrada[] = {"NO_CARROS", "CARROS_ESTE", "CARROS_NORTE", "AMBOS_LADOS"};

    //ESTADOS
    const char *estado[] = {"goNORTE", "waitNORTE","goESTE","waitESTE"};
    
	const char *entrada1[] = {"NO_CARROS", "CARROS_ESTE", "CARROS_NORTE", "AMBOS_LADOS"};
    char estado_actual[20];

    strcpy(estado_actual,"goNORTE");
    ASSERT_STREQ("goNORTE", estado_siguiente(entrada1, 0, estado_actual, estado, entrada));
    strcpy(estado_actual,"goNORTE");
    ASSERT_STREQ("waitNORTE", estado_siguiente(entrada1, 1, estado_actual, estado, entrada));
    strcpy(estado_actual,"goNORTE");
    ASSERT_STREQ("goNORTE", estado_siguiente(entrada1, 2, estado_actual, estado, entrada));
    strcpy(estado_actual,"goNORTE");
    ASSERT_STREQ("waitNORTE", estado_siguiente(entrada1, 3, estado_actual, estado, entrada));
    
    strcpy(estado_actual,"waitNORTE");
    ASSERT_STREQ("goESTE", estado_siguiente(entrada1, 0, estado_actual, estado, entrada));
    strcpy(estado_actual,"waitNORTE");
    ASSERT_STREQ("goESTE", estado_siguiente(entrada1, 1, estado_actual, estado, entrada));
    strcpy(estado_actual,"waitNORTE");
    ASSERT_STREQ("goESTE", estado_siguiente(entrada1, 2, estado_actual, estado, entrada));
    strcpy(estado_actual,"waitNORTE");
    ASSERT_STREQ("goESTE", estado_siguiente(entrada1, 3, estado_actual, estado, entrada));

    strcpy(estado_actual,"goESTE");
    ASSERT_STREQ("goESTE", estado_siguiente(entrada1, 0, estado_actual, estado, entrada));
    strcpy(estado_actual,"goESTE");
    ASSERT_STREQ("goESTE", estado_siguiente(entrada1, 1, estado_actual, estado, entrada));
    strcpy(estado_actual,"goESTE");
    ASSERT_STREQ("waitESTE", estado_siguiente(entrada1, 2, estado_actual, estado, entrada));
    strcpy(estado_actual,"goESTE");
    ASSERT_STREQ("waitESTE", estado_siguiente(entrada1, 3, estado_actual, estado, entrada));

    strcpy(estado_actual,"waitESTE");
    ASSERT_STREQ("goNORTE", estado_siguiente(entrada1, 0, estado_actual, estado, entrada));
    strcpy(estado_actual,"waitESTE");
    ASSERT_STREQ("goNORTE", estado_siguiente(entrada1, 1, estado_actual, estado, entrada));
    strcpy(estado_actual,"waitESTE");
    ASSERT_STREQ("goNORTE", estado_siguiente(entrada1, 2, estado_actual, estado, entrada));
    strcpy(estado_actual,"waitESTE");
    ASSERT_STREQ("goNORTE", estado_siguiente(entrada1, 3, estado_actual, estado, entrada));

}

int main(int argc, char **argv){
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
