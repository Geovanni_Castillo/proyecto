#include "maquina.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char  *estado_siguiente(const char *entrada2[], int index, const char *estado_actual, const char *estado[], const char *entrada[]){
    const char estado_actual_aux[10];

    if (strcmp(estado_actual,estado[0]) == 0){ //goNORTE
            
        if (strcmp(entrada2[index],entrada[0]) == 0 || strcmp(entrada2[index],entrada[2]) == 0){ //NO_CARROS - CARROS_NORTE
                
            strcpy(estado_actual_aux,estado[0]);
            //printf("%s <--- %s ---> %s\n", estado_actual, entrada2[index], estado_actual_aux);
            printf("%s \n", estado_actual_aux);
            strcpy(estado_actual,estado_actual_aux);

        }else if (strcmp(entrada2[index], entrada[1])== 0 || strcmp(entrada2[index],entrada[3]) == 0){ //CARROS_ESTE
                
            strcpy(estado_actual_aux,estado[1]);
            //printf("%s <--- %s ---> %s\n", estado_actual, entrada2[index], estado_actual_aux);
            printf("%s \n", estado_actual_aux);
            strcpy(estado_actual,estado_actual_aux);

        }else{
            printf("Error en goNORTE\n");
        }   
            
    }else if(strcmp(estado_actual,estado[1]) == 0){ //waitNORTE
            
        if (strcmp(entrada2[index],entrada[0]) == 0 || strcmp(entrada2[index],entrada[1]) == 0 || strcmp(entrada2[index],entrada[2]) == 0 || strcmp(entrada2[index],entrada[3]) == 0){ //NO_CARROS - CARROS_ESTE - CARROS_NORTE - AMBOS_LADOS
                
            strcpy(estado_actual_aux,estado[2]);
            //printf("%s <--- %s ---> %s\n", estado_actual, entrada2[index], estado_actual_aux);
            printf("%s \n", estado_actual_aux);
            strcpy(estado_actual,estado_actual_aux);

        }else{
            printf("Error En waitNORTE\n");
        }

    }else if(strcmp(estado_actual,estado[2]) == 0){ //goESTE
            
        if (strcmp(entrada2[index],entrada[0]) == 0 || strcmp(entrada2[index],entrada[1]) == 0){ //NO_CARROS - CARROS_ESTE
                
            strcpy(estado_actual_aux,estado[2]);
            //printf("%s <--- %s ---> %s\n", estado_actual, entrada2[index], estado_actual_aux);
            printf("%s \n", estado_actual_aux);
            strcpy(estado_actual,estado_actual_aux);

        }else if (strcmp(entrada2[index],entrada[2]) == 0 || strcmp(entrada2[index],entrada[3]) == 0){ //CARROS_NORTE - AMBOS_LADOS
                
            strcpy(estado_actual_aux,estado[3]);
            //printf("%s <--- %s ---> %s\n", estado_actual, entrada2[index], estado_actual_aux);
            printf("%s \n", estado_actual_aux);
            strcpy(estado_actual,estado_actual_aux);

        }else{
            printf("Error en goESTE");
        }

    }else if (strcmp(estado_actual,estado[3]) == 0){ //waitESTE
            
        if (strcmp(entrada2[index],entrada[0]) == 0 || strcmp(entrada2[index],entrada[1]) == 0 || strcmp(entrada2[index],entrada[2]) == 0 || strcmp(entrada2[index],entrada[3]) == 0){ //NO_CARROS - CARROS_ESTE - CARROS_NORTE - AMBOS_LADOS
                
            strcpy(estado_actual_aux,estado[0]);
            //printf("%s <--- %s ---> %s\n", estado_actual, entrada2[index], estado_actual_aux);
            printf("%s \n", estado_actual_aux);
            strcpy(estado_actual,estado_actual_aux);

        }else{
            printf("Error En waitESTE");
        }

    }else{
        printf("***ERROR***\n");
    }

    return estado_actual;
}


